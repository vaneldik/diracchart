# Expected variables from gitlab:
# * KRB_USERNAME: CERN account name to talk to Openshift
# * KRB_PASSWORD: CERN account pwd.
# * SSH_KEY: ssh key used to push back to gitlab
#
# Expected variables for the job
# * LHCB_DIRAC_VERSION: mandatory. Version of LHCbDIRAC to deploy
# * LHCB_DIRAC_SETUP: optional. Determines the kubernetes namespace which is used.
#                     If not specified, infer from LHCB_DIRAC_VERSION

image: gitlab-registry.cern.ch/ci-tools/ci-worker:c8

# This job will update the version number of LHCbDIRAC in the
# `values.yml` file of a specific environment.
# It needs access to Openstack to be able to obtain
# the master key used by sops
# See https://gitlab.cern.ch/helm/releases/gitops-getting-started/-/tree/master#secrets
# for detailed explanations about sops and secrets
update_version:
  # Only run when LHCB_DIRAC_VERSION variable is set, that is when
  # we are triggered by the LHCbDIRAC CI job
  only:
    variables:
      - $LHCB_DIRAC_VERSION
  before_script:
    # Setup whatever is needed to have openstack working
    - export OS_AUTH_URL=https://keystone.cern.ch/v3
    # This is the project ID corresponding to LHCb Orchestrator
    - export OS_PROJECT_ID=ea414adb-cb40-45d0-b86c-c59a08b1a9f6
    - export OS_PROJECT_NAME="LHCb Orchestrator"
    - export OS_USER_DOMAIN_NAME="Default"
    - export OS_PROJECT_DOMAIN_ID="default"
    - export OS_USERNAME=${KRB_USERNAME}
    - export OS_PASSWORD=${KRB_PASSWORD}
    - export OS_REGION_NAME="cern"
    - export OS_INTERFACE=public
    - export OS_IDENTITY_API_VERSION=3
    # init the dirac bot gitlab user
    - eval `ssh-agent -s`
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KEY" | tr -d '\r' > /root/.ssh/id_rsa
    - chmod 700 /root/.ssh/id_rsa
    - ssh-keyscan -p 7999 gitlab.cern.ch >> /root/.ssh/known_hosts
    - git config --global user.email "dirac.bot@cern.ch"
    - git config --global user.name "LHCbDIRAC Bot"
    - git config --global pull.rebase true
    # use SSH rather than https to be able to push back
    - git remote show origin
    - git remote set-url origin ssh://git@gitlab.cern.ch:7999/$CI_PROJECT_PATH
    - git remote show origin
  script:
    - set -x
    # Which kubernetes namespace are we changing
    # If LHCB_DIRAC_SETUP is given, use it, otherwise deduce it from
    # the version of DIRAC that one wants to install
    - |
      if [[ -z "${LHCB_DIRAC_SETUP:-}" ]]; then
        echo "Namespace not specified, chosing one from version ${LHCB_DIRAC_VERSION}";
        if [[ "${LHCB_DIRAC_VERSION}" == v+([0-9]).+([0-9]).+([0-9]) ]]; then
          LHCB_DIRAC_SETUP="production";
        else
          LHCB_DIRAC_SETUP="certification";
        fi;
      fi
    - helmValueFile="releases/${LHCB_DIRAC_SETUP}/values.yaml"
    # Check that the file exists
    - |
      if [[ ! -f "${helmValueFile}" ]]; then
        echo "${helmValueFile} does not exist, exiting";
        exit 1;
      fi
    # We need the CERN specific version of sops, see comment above
    - curl -O -L https://gitlab.cern.ch/cloud/sops/-/jobs/8834328/artifacts/raw/sops
    - chmod +x sops
    # Update the image tag
    - ./sops --set "['spec']['values']['image']['tag'] \"$LHCB_DIRAC_VERSION\"" ${helmValueFile}
    # And push back
    - git diff
    - git add ${helmValueFile}
    - git commit -m "Update ${helmValueFile} to ${LHCB_DIRAC_VERSION}"
    - git status
    - git branch
    - git push origin HEAD:${CI_COMMIT_REF_NAME}
