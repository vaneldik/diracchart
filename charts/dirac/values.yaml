# Default values for diracchart.
# Declare variables to be passed into your templates.

# This is the default number of replica
# for each DIRAC service
replicaCount: 1

namespace: "default"

# details on the image to use
image:
  repository: gitlab-registry.cern.ch/lhcb-dirac/lhcbdirac
  pullPolicy: IfNotPresent
  # Overrides the image tag whose default is the chart appVersion.
  tag: "latest"

# Definition of the DIRAC services we want to run.
# We create one service and one deployment per entry
# Note that the Port has to be the same as the one in the CS,
# but also the same as in the ingress definition (see below)
diracServices:
  # Name of the service (will be used for naming k8 services, pods, deployment, etc)
  #dfc:
  #  # Port to expose
  #  port: 9197
  #  # Argument to give to dirac-service to start it
  #  cmd: "DataManagement/FileCatalog"
  #rms:
  # port: 9140
  # cmd: "RequestManagement/ReqManager"


# Various environment variables are used to specify locations
# like the CRLs, etc
# Be careful that the paths must match the various mountpoints
# like ``/secrets`` or ``cvmfs_root``
environment:
  # This corresponds to the basic dirac.cfg
  # which must be present on all the servers
  DIRAC_CFG_COMMON: "/secrets/dirac.cfg"
  # Points to the host certificate
  DIRAC_X509_HOST_CERT: "/secrets/tls.crt"
  # Points to the host key
  DIRAC_X509_HOST_KEY: "/secrets/tls.key"
  # Certificate location
  X509_CERT_DIR: "/cvmfs_root/etc/grid-security/certificates"
  SSL_CERT_DIR: "/cvmfs_root/etc/grid-security/certificates"
  # Vomsdir location
  X509_VOMS_DIR: "/cvmfs_root/etc/grid-security/vomsdir"

# EXPERIMENTAL
# If true, use a real cvmfs CSI driver
# otherwise use a local directory
useCVMFS: true

# Options regarding the master CS
# Note: the secrets needed by the master
# are in the `secrets` section
masterCS:
  # If True, runs a master CS
  enabled: false

  # port on which to run
  # CAUTION: it has to be the same as what is
  # in the masterCfg secret.
  port: 9101

  # Mountpoint for the persistant storage
  # CAUTION: this place has to be the same as what is
  # in the masterCfg secret.
  persistantStorageMountPoint: /masterCS

  # Configuration for the storage used
  # to store the master CS files
  # This is specially crafted to run on CERN magnum
  # Best doc I found for the parameter description:
  # https://gitlab.ows.fr/public-project/cloud-provider-openstack/openstack-cloud-controller-manager/-/blob/v0.2.0/docs/using-manila-provisioner.md
  # For more info about the possible values at CERN:
  # https://clouddocs.web.cern.ch/containers/tutorials/cephfs.html#for-kubernetes-v121-and-higher
  cephFS:
    enabled: true
    type: "Geneva CephFS Testing"
    zones: "nova"
    osSecretName: "os-trustee"
    osSecretNamespace: "kube-system"
    osShareID: "something"
    osShareAccessID: "somethingElse"


# Options regarding the tornado services
tornadoServices:
  # If True, runs a tornado service
  enabled: false

  # port on which to run
  port: 8443

# All the secrets needed by the DIRAC services (any of them)
# will be defined here
secrets:

  # Certificate related information
  tls:
    # The host certificate that will be used
    # by all pods
    cert: |+
      -----BEGIN CERTIFICATE-----
      -----END CERTIFICATE-----
    # The key of the certificate that will be used
    # by all pods
    key: |+
      -----BEGIN RSA PRIVATE KEY-----
      -----END RSA PRIVATE KEY-----

  # Whatever goes in the dirac.cfg used by all the services
  # This corresponds to the content of the standard dirac.cfg
  # you have an all your servers
  diracCfg: |+
    DIRAC
    {
      Setup = LHCb-Kubernetes
      VirtualOrganization = lhcb
      Hostname = lhcb-dirac-k8.cern.ch
      Configuration
      {
        Servers = dips://dirac-k8.cern.ch:9135/Configuration/Server
      }
      Setups
      {
        LHCb-Kubernetes
        {
          Configuration = Certification
          Framework = Certification
        }
      }
    }
    LocalSite
    {
      Site = dirac-k8.cern.ch
    }
    Systems
    {
      Databases
      {

        User = myDBUser
        Password = myPassword
        Host = mydb.cern.ch
        Port = 5506

      }
    }

  # Extra cfg file needed by the master CS
  # to bootstrap
  masterCfg: |+
    DIRAC
    {
      Configuration
      {
        Master = yes
        Name =  /masterCS/Test-Config
      }
    }
    Systems
    {
      Configuration
      {
        Certification
        {
          Services
          {
            Server
            {
              HandlerPath = DIRAC/ConfigurationSystem/Service/ConfigurationHandler.py
              MaxThreads = 500
              Port = 9101
              Authorization
              {
                Default = all
                commitNewData = CSAdministrator
                rollbackToVersion = CSAdministrator
                getVersionContents = ServiceAdministrator
                getVersionContents += CSAdministrator
              }
            }
          }
        }
      }
    }




# This section is for configuring the ingress.
# Really, you should not play with this, unless you know what you are doing
# EXCEPT for the tcp section that you must change when adding a service

# Config inspired by what the CERN magnum guys have done
# https://gitlab.cern.ch/cloud-infrastructure/magnum/-/blob/cern/train/magnum/drivers/common/templates/kubernetes/helm/ingress-nginx.sh
ingress-nginx:
  # If enabled, deploy the ingress-nginx ourselves
  enabled: true
  controller:
    kind: DaemonSet
    # Don't ask why, I still need to understand
    hostNetwork: true
    # This should be commented if you want to run on minikube
    nodeSelector:
      # myingress is the role to give to the openstack nodes
      # https://clouddocs.web.cern.ch/containers/tutorials/lb.html#cluster-setup
      role: diracIngress
    #service:
    #  type: NodePort
    # This configuration is to change the default
    # port of the admission webhook to not use 8443
    # which is already used by DIRAC tornado
    admissionWebhooks:
      port: 8444

  # This is the section you need to edit whenever you add a DIRAC service.
  # This would also be the only part of the configuration you would need if
  # you were to rely on the CERN Magnum ingress
  tcp:
    # <external port>: <namespace>/<k8 service name>:<container port>
    # external port: port on which the pod will be accessible from outside
    # namespace: the namespace in which your helm chart is installed (managed by flux)
    # k8 service name: the name of the k8 service (defined in service.yaml), not the dirac service.
    #                  It should be something like <release-name>-<dirac service name>
    #                  e.g diracchart-certif-dfc
    # container port: the port exposed by the container, and defined in the CS
    "9197": default/canary-diracchart-dfc:9197

## ADVANCED
# Below are some default variables created by default by Helm.
# So far, we do not make any use of them

# These are used to override chart name. See `_helpers.tpl`
#nameOverride: ""
#fullnameOverride: ""
#
#serviceAccount:
#  # Specifies whether a service account should be created
#  create: true
#  # Annotations to add to the service account
#  annotations: {}
#  # The name of the service account to use.
#  # If not set and create is true, a name is generated using the fullname template
#  name: ""
#
#podAnnotations: {}
#
#podSecurityContext: {}
#  # fsGroup: 2000
#
#securityContext: {}
#  # capabilities:
#  #   drop:
#  #   - ALL
#  # readOnlyRootFilesystem: true
#  # runAsNonRoot: true
#  # runAsUser: 1000
#
#service:
#  type: NodeIP
#  port: 9197
#  nodePort: 30080
#
#ingress:
#  enabled: false
#  annotations: {}
#    # kubernetes.io/ingress.class: nginx
#    # kubernetes.io/tls-acme: "true"
#  hosts:
#    - host: chart-example.local
#      paths: []
#  tls: []
#  #  - secretName: chart-example-tls
#  #    hosts:
#  #      - chart-example.local
#
#resources: {}
#  # We usually recommend not to specify default resources and to leave this as a conscious
#  # choice for the user. This also increases chances charts run on environments with little
#  # resources, such as Minikube. If you do want to specify resources, uncomment the following
#  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
#  # limits:
#  #   cpu: 100m
#  #   memory: 128Mi
#  # requests:
#  #   cpu: 100m
#  #   memory: 128Mi
#
#autoscaling:
#  enabled: false
#  minReplicas: 1
#  maxReplicas: 100
#  targetCPUUtilizationPercentage: 80
#  # targetMemoryUtilizationPercentage: 80
#
#nodeSelector: {}
#
#tolerations: []
#
#affinity: {}
