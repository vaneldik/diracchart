
{{- $fullName := include "diracchart.fullname" . -}}
{{- $chartName := include "diracchart.name" . -}}
{{- $srvLabels := include "diracchart.labels" . }}

{{- if $.Values.masterCS.enabled }}

apiVersion: v1
kind: Service
metadata:
  name: {{ $fullName }}-master-cs
  labels:
    {{- $srvLabels | nindent 4 }}
spec:
  # The cluster IP must be none to make it a headless service
  # which is required for a Stateful set
  # https://kubernetes.io/docs/concepts/services-networking/service/#headless-services
  clusterIP: None
  selector:
    app: {{ $chartName }}-master-cs
    release: {{ $.Release.Name }}
  ports:
    - protocol: TCP
      port: {{ $.Values.masterCS.port }}
---

# The master CS is defined as a StatefulSet in order
# to make sure that there is only one running
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ $fullName }}-master-cs
  labels:
    app: {{ $chartName }}-master-cs
    release: {{ $.Release.Name }}
spec:
  replicas: 1
  serviceName: {{ $fullName }}-master-cs
  selector:
    matchLabels:
      app: {{ $chartName }}-master-cs
      release: {{ $.Release.Name }}
  template:
    metadata:
      labels:
        app: {{ $chartName }}-master-cs
        release: {{ $.Release.Name }}
    spec:
      terminationGracePeriodSeconds: 10
      containers:
      - name: {{ $.Chart.Name }} 
        image: "{{ $.Values.image.repository }}:{{ $.Values.image.tag | default $.Chart.AppVersion }}"
        imagePullPolicy: {{ $.Values.image.pullPolicy }}
        ports:
        - containerPort: {{ $.Values.masterCS.port }}
        env:
        - name: DIRAC_COMPONENT
          value: "Configuration/Server"
        # Add an extra configuration file 
        # specific to the master. It contains
        # basically what is needed for bootstrap
        - name: DIRAC_CFG_MASTER_CS
          value: "/secrets/master.cfg"
        envFrom:
        - configMapRef:
            name: env-config 
        volumeMounts:
        - mountPath: /secrets
          name: all-secrets-volume
          readOnly: true 
        - mountPath: /cvmfs_root
          name: cvmfs-pvc-mount
        # Mount the volume containing the actual
        # CS data
        - mountPath: {{ $.Values.masterCS.persistantStorageMountPoint | quote }}
          name: {{ $fullName }}-master-cs-data
      volumes:
      - name: all-secrets-volume
        projected:
          sources:
          - secret:
              name: tls-secret
          - secret:
              name: cfg-secret
          - secret:
              name: master-cs-cfg-secret
      - name: cvmfs-pvc-mount
        persistentVolumeClaim:
          claimName: cvmfs-pvc
  # This defines the volumeclaim generated for
  # the masterCS data
  # https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/#stable-storage
  volumeClaimTemplates:
  - metadata:
      name: {{ $fullName }}-master-cs-data
    spec:
      accessModes: [ "ReadWriteOnce" ]
      storageClassName: ""
      volumeName: master-cs-pv
      resources:
        requests:
          storage: 1G
{{- end }}
