# diraccart


## Table of content

 * [Introduction](#introduction)
 * [Getting access to the cluster](#getting-access-to-the-cluster)
 * [Changing the configuration](#changing-the-configuration)
 * [Testing locally](#testing-locally)
 * [Creating a production cluster](#creating-a-production-cluster)
 * [Technical details](#technical-details)


 ```bash
 # TOC created with
 IFS=$'\n\t'; for t in $(grep '^## ' README.md | tail -n+2 | sed 's/## //g'); do echo " * [${t}](#$(echo $t | tr  '[A-Z]' '[a-z]' | tr ' ' '-'))"; done
 ```


## Introduction

The ``DIRAC`` services have been packaged as a ``helm`` chart (under ``charts`` folder). Its deployment on a cluster is managed by ``flux v1``.

The basic idea of the DIRAC ``helm`` chart are the following:

* Each *standard* DIRAC service that is stateless and can be replicated is described as a kubernetes ``deployment`` (see [dirac_deployment.yaml](charts/dirac/templates/dirac_deployment.yaml). A kubernetes ``service`` is associated to each.
* Specific services, that require permanent storage or no more than a single instance, are described using kubernetes ``statefulset``.
* ``Tornado`` is defined in a different but very similar ``deployment`` as the *standard* DIRAC services (see [tornado_deployment.yaml](charts/dirac/templates/tornado_deployment.yaml))
* ``cvmfs`` is mounted on every ``Pod`` by a ``PersistentVolumeClaim`` (see [pvc-cvmfs](charts/dirac/templates/pvc-cvmfs.yaml))
* The content of the local ``dirac.cfg`` used by the services is managed by kubernetes ``Secret`` (see [cfg-secret.yaml](charts/dirac/templates/cfg-secret.yaml)) and defined in the ``values.yaml``
* As for ``dirac.cfg``, the host certificate and keys are defined in ``values.yaml`` and managed with a kubernetes ``Secret``
* Various environment variables are used to run the services and are common to all of them. They are managed with a kubernetes ``ConfigMap`` ( see [EnvConfig.yaml](charts/dirac/templates/EnvConfig.yaml))




## Getting access to the cluster

Anybody with openstack access to the ``LHCbOrchestrator`` project has access to the ``kubernetes`` clusters created there. Just setup the tenant environment, and [get the kubernetes config file](https://clouddocs.web.cern.ch/containers/tutorials/kubernetes-keystone-authentication.html#retrieve-the-administrator-credentials). For example

```bash
[chaen@lxplus8s15 ~]$ openstack coe cluster list
+--------------------------------------+-----------------+------------+------------+--------------+-----------------+---------------+
| uuid                                 | name            | keypair    | node_count | master_count | status          | health_status |
+--------------------------------------+-----------------+------------+------------+--------------+-----------------+---------------+
| 5afff58a-c710-4c15-8a8f-2032bb4e98bb | dcos-LHCb       | lxplus     |          5 |            3 | CREATE_COMPLETE | None          |
| 7ec86762-9795-461a-bc99-98b58e7294ad | lhcb-cert-dirac | lxplus-rsa |          5 |            1 | CREATE_COMPLETE | None          |
+--------------------------------------+-----------------+------------+------------+--------------+-----------------+---------------+

[chaen@lxplus8s15 ~]$ $(openstack coe cluster config lhcb-cert-dirac)

[chaen@lxplus8s15 ~]$ kubectl cluster-info
Kubernetes control plane is running at https://188.185.122.226:6443
CoreDNS is running at https://188.185.122.226:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

## Changing the configuration

Most of the time, changing the configuration will be about adding or removing a service. The configuration is stored in 2 places:

* [charts/dirac/values.yaml](charts/dirac/values.yaml): these are the default values, and contain a lot of explanations about how to tune the configuration. That file can be edited using your favourite text editor. Since these are the default values, that apply to all the installations, it is pretty unusual to have to play with this configuration.
* [releases/certification/values.yaml](releases/certification/values.yaml): this configuration is specific to the ``certification`` cluster. That file needs to be edited with `sops` (see below)

Update the configuration file as you see fit, push back to the repository, and you are all done, ``flux`` will take care of propagating the changes.

The operation is the same if you want to change the LHCbDIRAC version, or add more replicas.

### Example: adding a service

Let's assume we want to run and expose the ``RequestManagement/ReqManager`` service. This means that we need to:

* Add the service in the list of ``diracServices``
* Expose the port in the ingress

The list of services to run is specific to a cluster, so we need to change ``releases/certification/values.yaml``.

Login on ``lxplus8``, and follow the [setup guide for sops](https://gitlab.cern.ch/helm/releases/gitops-getting-started/-/tree/master/#setup). Open the configuration file with ``sops``

``` bash
sops releases/certification/values.yaml
```

Adding the following to ``diracServices``:

```yaml
        diracServices:
            # The name can be anything k8 permits, as long as its unique
            reqmanager:
                # port on which the dirac service listens to (same as in the CS)
                port: 9140
                cmd: RequestManagement/ReqManager
```

Open the port in the ingress section

```yaml
        ingress-nginx:
            tcp:
                # <externalPort>: <namespace>/<chart release name>-<diracService name>:<service port>
                "9140": certification/diracchart-certif-reqmanager:9140
```

Save, commit, and push. ``flux`` will take care of deploying.


## WebUI

### k8 dashboard

It's crap, don't use it, really.

You can access it using the default TLS port of your cluster. For example https://lhcb-cert-dirac-k8.cern.ch/dashbord/

The easiest is to authenticate using the token. You can get the token with

https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md#getting-a-bearer-token


```bash
kubectl -n kube-system get secret $(kubectl -n kube-system get sa/admin -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
```

### Grafana

You can access it here https://lhcb-cert-dirac-k8.cern.ch/

The grafana dashbord for prometheus requires username/password that was setup upon cluster creation.


## Testing locally

Running and testing a Helm chart locally, especially managed by Flux, is still an open question.

In an attempt to make that possible, some variables in ``values.yaml`` allow you to mock the *real thing* with a local equivalent. For example, you could mount ``cvmfs`` from a local folder instead of a proper ``csi`` driver.
All this is experimental, and is marked as such in ``values.yaml``. More docs to come when this expands.

## Creating a production cluster

Creating a cluster is done by following the [clouddocs documentation](https://clouddocs.web.cern.ch/containers/quickstart.html)

Of course, it requires you to already have [setup everything](https://clouddocs.web.cern.ch/tutorial/create_your_openstack_profile.html) to operate with openstack
As an example, this is how the certification cluster was created:


From `lxplus8` setup the environment for `LHCbOrchestrator` tenant.


Select the kubernetes cluster template you want. Ideally, we would want a multi-master cluster, but for the time being, due to some issues with LB quotas, we stick to the single master mode.


```bash
$ openstack coe cluster template list
+--------------------------------------+---------------------------+
| uuid                                 | name                      |
+--------------------------------------+---------------------------+
| 0931fccf-f73a-4c17-83e6-303a59b379c7 | dcos-preview-LHCb         |
| c6a485a5-1119-49e2-8c52-c2bed010a94d | kubernetes-LHCb           |
| c210cc47-b021-4acd-b572-74b156a1210a | dcos-preview-Chris        |
| 17760a5f-8957-4794-ab96-0d6bd8627282 | swarm-18.06-1             |
| ab08b219-3246-4995-bf76-a3123f69cb4f | swarm-1.13.1-2            |
| 200327fe-10b0-4685-96c1-ecda717638c2 | kubernetes-1.20.4-4       |
| 262992e0-459b-4183-a8bf-08e8af079c17 | kubernetes-1.20.4-4-multi |
| 0ae91ef4-4b30-432f-923f-a35d70160de4 | kubernetes-1.21.1-3       |
| a65602bb-4a54-45d4-805b-62dc5dbfef9c | kubernetes-1.21.1-3-multi |
| 55706780-29ef-4ec0-b77f-bfac67a7306b | kubernetes-1.22.3-4       |
| c164bd0f-9c95-48cd-a57f-0d144a8e80ad | kubernetes-1.22.3-4-multi |
+--------------------------------------+---------------------------+
```


There are a few options to specify upon creation:

```bash
export CLUSTER_NAME=lhcb-cert-dirac
export KUBERNETES_TEMPLATE=kubernetes-1.22.3-4
export GRAFANA_ADMIN_PASSWORD=1234
export NB_OF_NODES=5

```

The creation is done with the following command

```bash
openstack coe cluster create ${CLUSTER_NAME} \
                             --cluster-template ${KUBERNETES_TEMPLATE} \
                             --merge-labels \
                             --labels ingress_controller=false \
                             --labels monitoring_enabled=true \
                             --labels grafana_admin_passwd=${GRAFANA_ADMIN_PASSWORD} \
                             --labels logging_installer=helm \
                             --labels logging_producer=${CLUSTER_NAME} \
                             --keypair lxplus-rsa \
                             --node-count ${NB_OF_NODES}
```

A few details:

* `merge-labels`: mandatory. Keep the other labels present by default
* `ingress_controller`: normally, you would let openstack manage the ingress for you. However, because of the very specific setup we need for DIRAC (see below), we need to manage the ingress ourselves. So we disable it
* `monitoring_enabled` and `grafana_admin_passwd` enable [prometheus monitoring](https://clouddocs.web.cern.ch/containers/tutorials/prometheus.html)
* `logging_installer` and `logging_producer` are used for [centralized logging](https://clouddocs.web.cern.ch/containers/tutorials/logging.html)


Once the cluster creation is complete, we can export the cluster configuration file. This file can be kept somewhere (safe!!) and reuse later to talk to the cluster

```bash
openstack coe cluster config ${CLUSTER_NAME} && mv config ${CLUSTER_NAME}_k8.conf && export KUBECONFIG=$(realpath ${CLUSTER_NAME}_k8.conf)
```

The next step is to configure our slave nodes to also play the role of ingress. For that, we need to add them to the [DNS alias](https://clouddocs.web.cern.ch/using_openstack/properties.html) and given them a label in kubernetes. The alias would be `lhcb-cert-dirac-k8`


```bash
aliasNumber=1;
for node in $(kubectl get node -o go-template='{{range .items}}{{if eq (index .metadata.labels "magnum.openstack.org/role") "worker"}}{{.metadata.name}}{{"\n"}}{{end}}{{end}}');
do
  # Add the diracIngress label
  kubectl label node ${node} role=diracIngress;
  # Set the openstack property for the alias
  openstack server set --property landb-alias=${CLUSTER_NAME}-k8--load-${aliasNumber}- ${node};
  # increment the alias number
  ((aliasNumber+=1));
done
```

You need to wait for the DNS alias to actually exist before continuing.

Generate a [grid host certificate](https://ca.cern.ch/) for the alias (`lhcb-cert-dirac-k8.cern.ch`), and convert it to `pem` format. Keep it for later.


The next step is to configure `fluxcd` on your cluster. This is illustrated [here](https://gitlab.cern.ch/helm/releases/gitops-getting-started/-/tree/master/#secrets)


```bash
helm repo add fluxcd https://charts.fluxcd.io

kubectl create namespace flux

helm upgrade -i helm-operator fluxcd/helm-operator --namespace flux --values https://gitlab.cern.ch/helm/releases/gitops-getting-started/-/raw/master/helm-operator-values.yaml

helm upgrade -i flux fluxcd/flux --namespace flux \
    --version 1.12.0 \
    --set git.url=https://gitlab.cern.ch/lhcb-dirac/diracchart \
    --set git.branch=master \
    --values https://gitlab.cern.ch/helm/releases/gitops-getting-started/-/raw/master/flux-values.yaml
```

From this point, the ``helm`` chart will be installed on your cluster

### Firewall opening

Nodes in kubernetes need to be inserted in a specific set in landb. They can't reuse the standard dirac one. So you need to manually add your nodes in the [LHCB DIRAC3 K8 set](https://landb.cern.ch/landb/portal/sets/showSetInfo?setId=4294)
Be careful to insert only the nodes, and not the master.

### WebUI

There are two WebUI. The general kubernetes dashboard, and the prometheus grafana dashboard

#### K8 dashboard

> :warning: **That dashboard is crappy and does not really work**

CERN's cluster come [by default](https://clouddocs.web.cern.ch/containers/tutorials/monitoring.html) with the kubernetes dashboard setup.
It is possible to have some web dashboard for the cluster. By default the UI is installed on CERN's cluster.


We need however to expose it and this can be done by following [their guide](https://clouddocs.web.cern.ch/containers/tutorials/monitoring.html#using-traefik-ingress
) but customize it to our use of `nginx` as ingress.

HOWEVER, the `TLS Secret` needs to be in the same namespace as the `Ingress` and the `Ingress` needs to be in the same namespace as the `Backend`... For details see

* https://kubernetes.io/docs/concepts/services-networking/ingress/#resource-backend
* https://github.com/kubernetes/ingress-nginx/issues/2170


For Minikube: https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/


Customize the [dashboard file](cern-magnum/dashboard/dashboard.yaml) to your cluster, and apply it

First, copy the tls-secret. The namespace is the one generated by flux

``` bash
kubectl get secret tls-secret --namespace=certification -o yaml | sed 's/namespace: .*/namespace: kube-system/' | kubectl apply -f -
```

```bash
kubectl apply -f dashboard.yaml
```

You may want to customize the path to access the dashboard. In any case, the path should not conflict with the one used for prometheus

#### Prometheus grafana dashboard

Just customize the [prometheus file](cern-magnum/prometheus/prometheus.yaml) to your need, and apply it.


```bash
kubectl apply -f prometheus.yaml
```

## Technical details


### Docker image

The image used is defined in ``values.yaml``. We rely on the `lhcbdirac` [images](https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/container_registry/)

### Secrets

Configuration and certificates are managed as K8 ``secrets``. They are all mounted as files in a specific directory (``/secrets``) and we rely on environment variables to point DIRAC to them. Special care should be ensured that the environment variables ( in ``values.yaml``) and the mount path of the secrets (in the various deployments) remain consistent.

### Environment variables

Most environment variables that we [define](charts/dirac/templates/EnvConfig.yaml) are just overriding the default that are in the ``bashrc`` generated by ``DIRAC``. A special type of variables are all those starting with ``DIRAC_CFG``, which will be used to point to configuration files, and will be used by the ``LHCbDIRAC`` docker image (see [EntryPoint](https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/-/blob/master/container/lhcbdirac/dockerEntrypoint.sh#L45-56))

### TLS configuration

All the Pods will use the same host certificate, that should correspond to the alias defined when creating the cluster.

### CVMFS

We mount CVMFS to use the vomsdir, CAs and CRLs

### masterCS configuration

The master CS runs as a ``StatefulSet`` to make sure that only one is running. It has a dedicated section in ``values.yaml`` as well as a related configuration in the ``Secrets`` section

### Ingress configuration for DIRAC

DIRAC has the particularity to need the TLS encryption from the client till the DIRAC Service Pod. This is because we rely on X509 for client authentication. Thus we need an ingress working in passthrough mode. This is why we use ``ingress-nginx``, as opposed to the default ``traefik`` at CERN.

We have two options for having our ingress setup:

* Either we rely on the CERN magnum infrastructure to have it deployed, and then we would just need to specify the open ports (see [clouddocs](https://clouddocs.web.cern.ch/containers/tutorials/lb.html#ingress-ssl-passthrough))
* Or we deploy the ingress ourselves, which is relatively easy using Helm. This option has the advantage of allowing to run locally as well, since everything is self contained

### FluxCD

For the time being, we still use ``flux v1``, which is deprecated. We will in the future migrate either to ``flux v2`` or ``ArgoCD``, depending on CERN's recommendations (in the design process).

CERN provides a nice [getting started guide](https://gitlab.cern.ch/helm/releases/gitops-getting-started/-/tree/master/)

The basic idea is that you will have the ``values.yaml`` on a repository, two pods will be running on your cluster (``flux`` and ``helm-operator`` in the ``flux`` namespace), and will watch any change in the repo. When that occurs, these changes are propagated to the cluster.

You can have multiple releases (like ``certification`` and ``production``) running on a single cluster, or you can run one release per cluster, or even have multiple cluster running the same release.

The secrets are managed with ``sops`` with a special ``barbican`` plugin.

If you want your cluster to install the helm chart from a different branch than the ``master`` one, you need to specify it in two places:

* when installing flux (or editing it in place) with the ``--set git.branch=myBranch`` option
* in the ``values.yaml`` file corresponding to your release ``ref: myBranch``

### Automatic releases

This repository has a CI which will just update the ``LHCbDIRAC`` version specified in the appropriate ``values.yaml``. Although it can be ran by hand, it is automatically triggered when there is a tag in the [LHCbDIRAC repository](https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/-/blob/master/.gitlab-ci.yml#L338-353)


### Permission management

At the moment, there are no roles defined in the cluster. There is only the cluster admin account, and everybody with access to the ``LHCbOrchestrator`` openstack tenant can access it.

However, as the configuration is hosted on the gitlab repository, this is how one effectively manages the permissions.
